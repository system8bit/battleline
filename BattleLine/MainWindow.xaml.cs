﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BattleLine
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        TextBlock[] myCardControls = null;
        Dictionary<char, StackPanel> myFieldStackPanels = null;
        Dictionary<char, StackPanel> enemyFieldStackPanels = null;

        static String fieldDefinitions = "abcdefghi";

        Dictionary<char, SolidColorBrush> colorMap = new Dictionary<char, SolidColorBrush>();

        ObservableCollection<CardGridItem> items;

        SolidColorBrush grayBrash = new SolidColorBrush(Colors.Gray);


        Regex putRegex = new Regex(@"^PUT\s(?<Field>[a-i])\s(?<Card>[R|Y|P|O|G|B][0-9])$");
        Regex getRegex = new Regex(@"^GET\s(?<Field>[a-i])$");
        Regex cardRegex = new Regex(@"^CARD\s(?<Card>[R|Y|P|O|G|B][0-9])$");
        Regex doneRegex = new Regex(@"^DONE$");
        Regex resultRegex = new Regex(@"^YOU\s(?<Result>(WIN|LOSE))$");
        Regex messageRegex = new Regex(@"^MESSAGE\s(?<Message>.*?)$");


        string put = "PUT {0} {1}\r\n";
        string get = "GET {0}\r\n";
        string done = "DONE\r\n";

        private Color ToWhity(Color color)
        {
            return Color.Multiply(color, 0.75f);
        }
        public MainWindow()
        {
            InitializeComponent();

            colorMap.Add('R', new SolidColorBrush(ToWhity(Colors.Red)));
            colorMap.Add('P', new SolidColorBrush(ToWhity(Colors.Purple)));
            colorMap.Add('Y', new SolidColorBrush(ToWhity(Colors.Yellow)));
            colorMap.Add('O', new SolidColorBrush(ToWhity(Colors.Orange)));
            colorMap.Add('B', new SolidColorBrush(ToWhity(Colors.Blue)));
            colorMap.Add('G', new SolidColorBrush(ToWhity(Colors.Green)));
            
            items = new ObservableCollection<CardGridItem>();

            this.GridItems.ItemsSource = items;

            var myFieldSeq = new []{ myField0, myField1, myField2, myField3, myField4, myField5, myField6, myField7, myField8 };
            var enemyFieldSeq = new[] { enemyField0, enemyField1, enemyField2, enemyField3, enemyField4, enemyField5, enemyField6, enemyField7, enemyField8 };
           
            myCardControls = new TextBlock[] { myCard0, myCard1, myCard2, myCard3, myCard4, myCard5, myCard6 };
            myFieldStackPanels = new Dictionary<char,StackPanel>();
            enemyFieldStackPanels = new Dictionary<char,StackPanel>();

            Combine(fieldDefinitions, myFieldSeq).ForEach(x => myFieldStackPanels.Add(x.Key, x.Value));
            Combine(fieldDefinitions, enemyFieldSeq).ForEach(x => enemyFieldStackPanels.Add(x.Key, x.Value));
        }

        public IEnumerable<KeyValuePair<K, V>> Combine<K, V>(IEnumerable<K> a, IEnumerable<V> b)
        {
            var length = Math.Max(a.Count(), b.Count());

            return Enumerable.Range(0, length).Select(x => new KeyValuePair<K, V>(a.ElementAtOrDefault(x), b.ElementAtOrDefault(x)));
        }

        class CardGridItem
        {
            public Brush Background { get; set; }
            public bool[] Numbers { get; set; }
            public int Row { get; set; }
            public int Column { get; set; }
        }

        enum Flag
        {
            Pending,
            Enemy,
            Mine
        }

        class CardHolder
        {
            Dictionary<char, bool[]> fieldDictionary = new Dictionary<char, bool[]>();

            public CardHolder()
            {
                "RYPOGB".ForEach(x => fieldDictionary.Add(x, Enumerable.Repeat(false, 10).ToArray()));
            }

            public void add(string cardStr)
            {
                add(cardStr[0], cardStr[1]);
            }

            public void add(char initial, int number)
            {
                var cardArray = fieldDictionary[initial];
                cardArray[number] = true;
            }
        }

        class Field
        {
            public Dictionary<char, List<string>> MyField { get; private set; }
            public Dictionary<char, List<string>> EnemyField { get; private set; }

            public Dictionary<char, Flag> Flags { get; private set; }

            public Field()
            {
                MyField = new Dictionary<char, List<string>>();
                EnemyField = new Dictionary<char, List<string>>();
                Flags = new Dictionary<char, Flag>();

                fieldDefinitions.ForEach(x =>
                {
                    MyField.Add(x, new List<string>(3));
                    EnemyField.Add(x, new List<string>(3));
                    Flags.Add(x, Flag.Pending);
                });
            }
        }

        class Tefuda
        {
            public List<string> myCards { get; set; }
            public Tefuda(IEnumerable<string> cards)
            {
                myCards = new List<string>();
                cards.ForEach(x => myCards.Add(x));
            }
        }

        async private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            init();
            var client = new TcpClient();
            await client.ConnectAsync(IPAddress.Parse("176.32.89.187"), 8080);

            Field field = new Field();

            using (var ns = client.GetStream())
            using (var sr = new StreamReader(ns))
            using (var sw = new StreamWriter(ns))
            {
                var cardArray = await LoginSerer(sr, sw);
                var isFirst = cardArray[0] == "SENTE";
                var tefuda = new Tefuda(cardArray.Skip(1));

                SetTefuda(tefuda);

                if (!isFirst)
                {
                    await ReadServer(sr, field);
                }

                while (true)
                {
                    if (await WriteServer(sr, sw, tefuda, field))
                    {
                        await GetCard(sr, tefuda);
                    }
                    if (!await ReadServer(sr, field))
                    {
                        break;
                    }
                }
            }



        }

        void init()
        {
            items.Clear();
            Enumerable.Range(0, 10).ForEach(x =>
            {
                items.Add(new CardGridItem { Background = colorMap['R'], Numbers = new bool[10], Column = x, Row = 0 });
                items.Add(new CardGridItem { Background = colorMap['P'], Numbers = new bool[10], Column = x, Row = 1 });
                items.Add(new CardGridItem { Background = colorMap['Y'], Numbers = new bool[10], Column = x, Row = 2 });
                items.Add(new CardGridItem { Background = colorMap['O'], Numbers = new bool[10], Column = x, Row = 3 });
                items.Add(new CardGridItem { Background = colorMap['B'], Numbers = new bool[10], Column = x, Row = 4 });
                items.Add(new CardGridItem { Background = colorMap['G'], Numbers = new bool[10], Column = x, Row = 5 });
            });
            myFieldStackPanels.Values.ForEach(x => x.Children.Clear());
            enemyFieldStackPanels.Values.ForEach(x => x.Children.Clear());
            InitField();
        }

        async Task<String> ReadLineSafe(StreamReader sr)
        {
            int count = 0;
            while (count < 10)
            {
                await Task.Delay(100);
                try
                {
                    var test =  await sr.ReadLineAsync();
                    if (test == null) return "";
                    this.LogBox.Text += test + "\r\n";
                    this.LogBox.ScrollToEnd();
                    return test;

                }
                catch (InvalidOperationException)
                {

                }
                count++;
            }

            throw new TimeoutException();

        }

        async Task GetCard(StreamReader sr, Tefuda tefuda)
        {
            var cardMessage = await ReadLineSafe(sr);

            if (cardRegex.IsMatch(cardMessage))
            {
                var cardMatch = cardRegex.Match(cardMessage);

                var card = cardMatch.Groups["Card"].Value;
                tefuda.myCards.Add(card);
            }
            else
            {

            }

            SetTefuda(tefuda);
        }

        void InitField()
        {
            LineAPendingButton.IsChecked = true;
            LineBPendingButton.IsChecked = true;
            LineCPendingButton.IsChecked = true;
            LineDPendingButton.IsChecked = true;
            LineEPendingButton.IsChecked = true;
            LineFPendingButton.IsChecked = true;
            LineGPendingButton.IsChecked = true;
            LineHPendingButton.IsChecked = true;
            LineIPendingButton.IsChecked = true;
        }

        void GetMyField(char field)
        {
            switch (field)
            {
                case 'a':
                    LineAMineButton.IsChecked = true;
                    break;
                case 'b':
                    LineBMineButton.IsChecked = true;
                    break;
                case 'c':
                    LineCMineButton.IsChecked = true;
                    break;
                case 'd':
                    LineDMineButton.IsChecked = true;
                    break;
                case 'e':
                    LineEMineButton.IsChecked = true;
                    break;
                case 'f':
                    LineFMineButton.IsChecked = true;
                    break;
                case 'g':
                    LineGMineButton.IsChecked = true;
                    break;
                case 'h':
                    LineHMineButton.IsChecked = true;
                    break;
                case 'i':
                    LineIMineButton.IsChecked = true;
                    break;
            }
        }

        void GetEnemyField(char field)
        {
            switch (field)
            {
                case 'a':
                    LineAEnemyButton.IsChecked = true;
                    break;
                case 'b':
                    LineBEnemyButton.IsChecked = true;
                    break;
                case 'c':
                    LineCEnemyButton.IsChecked = true;
                    break;
                case 'd':
                    LineDEnemyButton.IsChecked = true;
                    break;
                case 'e':
                    LineEEnemyButton.IsChecked = true;
                    break;
                case 'f':
                    LineFEnemyButton.IsChecked = true;
                    break;
                case 'g':
                    LineGEnemyButton.IsChecked = true;
                    break;
                case 'h':
                    LineHEnemyButton.IsChecked = true;
                    break;
                case 'i':
                    LineIEnemyButton.IsChecked = true;
                    break;
            }
        }

        async Task<string[]> LoginSerer(StreamReader sr, StreamWriter sw)
        {
            var hello = await ReadLineSafe(sr);

            await sw.WriteAsync(string.Format("LOGIN {0} {1}\r\n", "ke-suzuki@melcoinc.co.jp", "LrCpXoRo"));
            await sw.FlushAsync();

            var login = await ReadLineSafe(sr);

            var cards = await ReadLineSafe(sr);

            return cards.Split(' ');
        }

        async Task<bool> WriteServer(StreamReader sr, StreamWriter sw, Tefuda tefuda, Field field)
        {
            try
            {
                var yourTurn = await ReadLineSafe(sr);
                if (yourTurn != "YOUR TURN")
                {
                    if (resultRegex.IsMatch(yourTurn))
                    {
                        return false;
                    }


                    throw new Exception();
                }


                var pendigField = field.Flags.Where(x => x.Value == Flag.Pending);
                var myPendighField = pendigField.Select(x => new { f = x.Key, line = field.MyField[x.Key] }).Where(x => x.line.Count() != 3);
                var canPut = myPendighField.Any();
                if (canPut)
                {
                    char putField = '\0';
                    string putCard = "";

                    var putFields = myPendighField.Where(x => x.line.Count() != 0);
                    var putCards = putFields.Where(x => IsSameColor(x.line)).Select(x => new { f = x.f, c = x.line.First()[0] }).SelectMany(x => tefuda.myCards.Where(y => y[0] == x.c), (x, y) => new { Field = x.f, Card = y });
                    if (putCards.Any())
                    {
                        putField = putCards.First().Field;
                        putCard = putCards.First().Card;
                    }
                    else
                    {
                        var maxCard = GetMyFirstMaxCard(tefuda);
                        var myEmptyField = myPendighField.Where(x => x.line.IsEmpty());

                        if (myEmptyField.Any())
                        {
                            putField = myEmptyField.First().f;
                        }
                        else
                        {
                            putField = myPendighField.First().f;
                        }

                        putCard = maxCard;
                    }
                    await sw.WriteAsync(string.Format(put, putField, putCard));
                    field.MyField[putField].Add(putCard);
                    tefuda.myCards.Remove(putCard);

                    AddTextBlock(myFieldStackPanels[putField], putCard);
                }

                var pendigFieldNames = field.Flags.Where(x => x.Value == Flag.Pending).Select(x => x.Key);

                var peindingAndMaxCardField = pendigFieldNames.Where(x => field.MyField[x].Count() == 3 && field.EnemyField[x].Count() == 3);

                List<char> getFields = null;
                foreach (var x in peindingAndMaxCardField)
                {
                    if (CheckField(field.MyField[x], field.EnemyField[x]))
                    {
                        if (getFields == null) getFields = new List<char>();
                        char getMyField = x;
                        await sw.WriteAsync(string.Format(get, getMyField));

                        getFields.Add(getMyField);

                        GetMyField(getMyField);
                    }
                }

                if (peindingAndMaxCardField.Any())
                {
                    this.LogBox.Text += peindingAndMaxCardField.Select(x => x.ToString()).Aggregate((x, y) => x + y) + "\r\n";
                }

                if (getFields != null)
                {
                    getFields.ForEach(x =>
                    {
                        field.Flags[x] = Flag.Mine;
                    });
                }
                await sw.WriteAsync(done);

                await sw.FlushAsync();

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void AddTextBlock(StackPanel panel, string card)
        {
            var textBlock = new TextBlock();
            textBlock.TextAlignment = TextAlignment.Center;
            textBlock.Text = card[1].ToString();
            textBlock.Background = colorMap[card[0]];
            panel.Children.Add(textBlock);
            RemoveGrid(card);
        }

        void RemoveGrid(string card)
        {
            int magnification = 0;
            //if (card[0] == 'R') magnification = 0;
            if (card[0] == 'P') magnification = 1;
            else if (card[0] == 'Y') magnification = 2;
            else if (card[0] == 'O') magnification = 3;
            else if (card[0] == 'B') magnification = 4;
            else if (card[0] == 'G') magnification = 5;

            int number = card[1] - '0';

            int index = number + magnification * 10;

            var newItem = items[index];
            items.RemoveAt(index);
            items.Add(null);
            
        }


        bool IsDefeat(Field field)
        {
            var flags = field.Flags.Values.ToArray();
            for (int i = 0; i < flags.Count() - 3; i++)
            {
                bool seq3 = 
                    flags[i] == Flag.Mine && 
                    flags[i + 1] == Flag.Mine &&
                    flags[i + 2] == Flag.Mine;

                if (seq3)
                {
                    return true;
                }
            }


            return  field.Flags.Where(x => x.Value == Flag.Mine).Count() > 4;


        }

        string GetMyFirstMaxCard(Tefuda tefuda)
        {
            return tefuda.myCards.OrderBy(x => -(int)x[1]).First();
        }

        char GetMyFirstEmptyField(Field field)
        {
            return fieldDefinitions.Where(x => field.Flags[x] == Flag.Pending).FirstOrDefault();
        }

        

        async Task<bool> ReadServer(StreamReader sr, Field field)
        {
            while (true)
            {
                var serverMessage = await ReadLineSafe(sr);
                
                if (putRegex.IsMatch(serverMessage))
                {
                    var match = putRegex.Match(serverMessage);
                    var putField = match.Groups["Field"].Value[0];

                    var putCard = match.Groups["Card"].Value;

                    field.EnemyField[putField].Add(putCard);

                    AddTextBlock(enemyFieldStackPanels[putField], putCard);
                }
                else if (getRegex.IsMatch(serverMessage))
                {
                    var match = getRegex.Match(serverMessage);
                    var enemyGetField = match.Groups["Field"].Value[0];
                    field.Flags[enemyGetField] = Flag.Enemy;

                    GetEnemyField(enemyGetField);
                }
                else if (doneRegex.IsMatch(serverMessage))
                {
                    break;
                }
                else if (resultRegex.IsMatch(serverMessage))
                {
                    var match = resultRegex.Match(serverMessage);
                    var result = match.Groups["Result"].Value;

                    var line2 = await ReadLineSafe(sr);
                    var match2 = messageRegex.Match(line2);
                    var message = match.Groups["Message"].Value;

                    MessageBox.Show(result + ":" + message);
                    return false;
            
                }
                else if (messageRegex.IsMatch(serverMessage))
                {
                    var match = messageRegex.Match(serverMessage);
                    var message = match.Groups["Message"].Value;

                    if (!string.IsNullOrWhiteSpace(message))
                    {
                        MessageBox.Show(message);
                    }
                    return false;
                }
                else
                {
                    throw new Exception();
                }
            }
            return true;
        }

        private bool CheckField(IEnumerable<string> my, IEnumerable<string>  enemy)
        {
            bool isMySerial = IsSerial(my);
            bool isMySameColor = IsSameColor(my);

            bool isEnemySerial = IsSerial(enemy);
            bool isEnemySameColor = IsSameColor(enemy);

            int myMax = CardSum(my);
            int enemyMax = CardSum(enemy);

            bool isMax = myMax > enemyMax;

            if ((isMySerial & isMySameColor) && (isEnemySerial & isEnemySameColor))
            {
                return isMax;
            }
            else if ((isMySerial & isMySameColor) && !(isEnemySerial & isEnemySameColor))
            {
                return true;
            }

            bool isMySameNumber = IsSameNumber(my);
            bool isEnemySameNuber = IsSameNumber(enemy);

            if (isMySameNumber && isEnemySameNuber)
            {
                return isMax;
            }
            else if (isMySameNumber && !isEnemySameNuber)
            {
                return true;
            }

            if (isMySameColor && isEnemySameColor)
            {
                return isMax;

            }
            else if (isMySameColor && !isEnemySameColor)
            {
                return true;
            }

            return isMax;
        }

        private bool IsSameNumber(IEnumerable<string> cards)
        {
            return cards.Select(x => x[1]).Distinct().Count() == 1;
        }

        private bool IsSameColor(IEnumerable<string> bills)
        {
            return bills.Select(x => x[0]).Distinct().Count() == 1;
        }

        private bool IsSerial(IEnumerable<string> cards)
        {
            var serial =  cards.Select(x => x[1]).Select(x => x - '0').OrderBy(x => x);
            return serial.Select((x, i) => x - i).Distinct().Count() == 1;
        }

        private int CardSum(IEnumerable<string> cards)
        {
            return cards.Select(x => x[1]).Select(x => x - '0').Sum();
        }

        private int CardMax(IEnumerable<string> cards)
        {
            return cards.Select(x => x[1]).Select(x => x - '0').Max();
        }

        private void SetTefuda(Tefuda tefuda)
        {
            var xx = myCardControls.Select((x, i) => new { Index = i, X = x });
            var yy = tefuda.myCards.Select((x, i) => new { Index = i, X = x });

            var r = xx.GroupJoin(yy, x => x.Index, y => y.Index, (x, z) => new { b = x.X, c = z.DefaultIfEmpty() }).SelectMany(x => x.c, (x, a) => new { Button = x.b, Card = a });

            r.ForEach(x =>
            {
                if (x.Card != null)
                {
                    x.Button.Text = x.Card.X[1].ToString();
                    x.Button.Background = colorMap[x.Card.X[0]];
                }
                else
                {

                    x.Button.Text = "";
                    x.Button.Background = grayBrash;
                }
            });
            //var cardControls = Enumerable.Range(0, 7).Select(x => String.Format("MyCard{0}", x)).Select(x => this.GetType().GetProperty(x).GetValue(this)).Select(x => (Button)x).ToArray();


        }
    }
}
